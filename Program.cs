﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();


            ////ZADATAK 1 i 3
            //for (int i = 0; i < 20; ++i)
            //{
            //    diceRoller.InsertDie(new Die(6));
            //}


            ////ZADATAK 2.
            Random random = new Random();
            for (int i = 0; i < 20; ++i)
            {
                diceRoller.InsertDie(new Die(6, random));
            }



            diceRoller.RollAllDice();
            foreach (int rollResult in diceRoller.GetRollingResults())
            {
                Console.WriteLine(rollResult);
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace LV2
{
    class Die
    {
        private int numberOfSides;


        ////ZADATAK 1 i 2
        private Random randomGenerator;

        ////ZADATAK 1.
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}

        //ZADATAK 2.
        public Die(int numberOfSides, Random randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }



        ////ZADATAK 1 i 2 
        public int Roll()
        {
            int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            return rolledNumber;
        }


        // ZADATAK 3.
        //private RandomGenerator randomGenerator;
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = RandomGenerator.GetInstance();
        //}


        //public int Roll()
        //{
        //    int rolledNumber = this.randomGenerator.NextInt(1, numberOfSides + 1);
        //    return rolledNumber;
        //}

    }
}
